import * as sass from 'sass';
import { writeFileSync } from 'fs';
import debug from 'debug';

const log = debug('genCSS');
sass.render({
  file: './static/scss/main.scss',
  includePaths: [
    'node_modules/normalize-scss/sass',
  ],
}, (err, res): void => {
  if (err) throw err;
  writeFileSync('./static/main.css', res.css);
  log('CSS generated and writed.');
});
