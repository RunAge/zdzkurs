import * as express from 'express';
import * as http from 'http';
import debug from 'debug';

const log = debug('ServerHTTP');
const app = express();
const server = http.createServer(app);

app.use(express.static('./static'));
server.listen(8000, 'localhost', (): void => {
  log('Server starterd at port 8000');
});
