"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sass = require("sass");
const fs_1 = require("fs");
const debug_1 = require("debug");
const log = debug_1.default('genCSS');
sass.render({
    file: './static/scss/main.scss',
    includePaths: [
        'node_modules/normalize-scss/sass',
    ],
}, (err, res) => {
    if (err)
        throw err;
    fs_1.writeFileSync('./static/main.css', res.css);
    log('CSS generated and writed.');
});
