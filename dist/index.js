"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const http = require("http");
const debug_1 = require("debug");
const log = debug_1.default('ServerHTTP');
const app = express();
const server = http.createServer(app);
app.use(express.static('./static'));
server.listen(8000, 'localhost', () => {
    log('Server starterd at port 8000');
});
