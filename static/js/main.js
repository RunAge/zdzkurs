const App = {
  template: `
  <div id="app">
    <hero></hero>
    <router-view></router-view>
  </div>
  `
}

const Navbar = {
  template: `
  <div class="navbar">
    <router-link class="btn" to="/">Home</router-link>
    <router-link class="btn" to="/about">About</router-link>
    <router-link class="btn" to="/kofi">Ko-Fi</router-link>
    <router-link class="btn" to="/spotify">Spotify</router-link>
  </div>`
}

const Hero = {
  template: `
  <div class="hero">
    <h1>Kamide.re</h1>
    <navbar></navbar>
  </div>
  `
};

Vue.component('navbar', Navbar);
Vue.component('hero', Hero);
Vue.component('app', App);

const Home = { 
  template: `
    <div id="home">
      Home
    </div> 
  `
 };
const About = { template: '<p>About</p>' };
const Kofi = { template: '<p>KoFi</p>' };
const Spotify = { template: '<p>Spotify</p>' };

const routes = [
  { path: '/', component: Home },
  { path: '/about', component: About },
  { path: '/kofi', component: Kofi },
  { path: '/spotify', component: Spotify },
];

const router = new VueRouter({
  routes,
  linkExactActiveClass: 'active',

});

const vue = new Vue({
  el: '#app',
  router,
  template: `
  <app></app>
  `
})
