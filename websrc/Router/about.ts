import About from '../pages/About.vue';

export default {
  path: '/about',
  component: About,
}
