import Spotify from '../pages/Spotify.vue';

export default {
  path: '/spotify',
  component: Spotify,
}
