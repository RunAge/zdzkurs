import VueRouter from 'vue-router';
import Home from './home';
import About from './about';
import Kofi from './kofi';
import Spotify from './spotify';

const routes = [
  Home,
  About,
  Kofi,
  Spotify,
];

const router = new VueRouter({
  routes,
  linkExactActiveClass: 'active',
})

export default router;
