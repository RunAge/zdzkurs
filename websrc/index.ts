import Vue from 'vue';
import router from './Router';
import App from './App.vue';

new Vue({
  el: '#app',
  template: `<App/>`,
  router,
  components: {
    App,
  }
});
